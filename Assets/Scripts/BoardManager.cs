﻿/*
 * BoardManager
 * Copyright (c) Alessio Zap Boerio
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BoardManager: MonoBehaviour, IMapDelegate
{
    public Map map;
    public GameObject commandPanel;
    
    void Start()
    {
        map.mapDelegate = this;
        map.ConfigureMap();

        HideCommandPanel();
    }

    #region private functions

    private void ShowCommandPanelOn(Transform cell)
    {
        commandPanel.transform.position = cell.position;
        commandPanel.transform.localScale = Vector3.zero;
        commandPanel.SetActive(true);
        commandPanel.transform.DOScale(new Vector3(1f,1f,1f), 0.2f);
    }

    private void HideCommandPanel()
    {
        commandPanel.SetActive(false);
    }

    #endregion

    #region IMapDelegate implementation

    void IMapDelegate.MapCellDidTap(MapCell mapCell)
    {
        if (mapCell.champion != null)
        {
            ShowCommandPanelOn(mapCell.transform);
        }
    }

    void IMapDelegate.OutsideOfMapDidTap()
    {
        HideCommandPanel();
        map.DeselectCells();
    }

    #endregion


}

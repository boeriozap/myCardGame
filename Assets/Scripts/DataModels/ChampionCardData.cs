﻿/*
 * SpellCardData
 * Copyright (c) Alessio Zap Boerio
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class ChampionCardData : JsonDecodable
{
    [Header("General info")]
    public string title = "";
    public ClanColor clanColor = ClanColor.Green;

    [TextArea(2,3)]
//    public string description = "";
    public string imageUrl = "";
//    public int manaCost = 0;
    //    public CardRarity rarity;

    [Header("Champion Info")]
    public int maxToughness;
    public int power;
    public int movement = 2;
//    public int attackRange = 1;
//    public string CreatureScriptName;

//    [Header("Passive skill")]
//    public ChampionPassiveSkill passiveSkill;
//    public ChampionActiveSkill secondarySkill;
//    public ChampionActiveSkill mainSkill;

//    [Header("SpellInfo")]
//    public string spellScriptName;
//    public TargetingOptions targets;

    public ChampionCardData()
    {
        this.title = "";
        this.clanColor = ClanColor.Green;
        this.imageUrl = "";

        this.maxToughness = 1;
        this.power = 1;
        this.movement = 2;
    }

    public ChampionCardData(string title, ClanColor clanColor, string imageUrl, int maxToughness, int power, int movement)
    {
        this.title = title;
        this.clanColor = clanColor;
        this.imageUrl = imageUrl;

        this.maxToughness = maxToughness;
        this.power = power;
        this.movement = movement;
    }

    #region implemented abstract members of JsonDecodable

    public override void Init(JsonData j)
    {
        this.title = (string)j["title"];
        this.clanColor = (ClanColor)((int)j["clan_color"]);
        this.imageUrl = (string)j["image_url"];

        this.maxToughness = (int)j["max_toughness"];
        this.power = (int)j["power"];
        this.movement = (int)j["movement"];
    }

    #endregion
}

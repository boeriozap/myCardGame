﻿/*
 * DeckData
 * Copyright (c) Alessio Zap Boerio
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;


public class DeckData : JsonDecodable
{
    public List<SpellCardData> spells;
    public List<ChampionCardData> champions;

    public DeckData()
    {
        this.spells = new List<SpellCardData>();
        this.champions = new List<ChampionCardData>();
    }

    public DeckData(List<SpellCardData> spells, List<ChampionCardData> champions)
    {
        this.spells = spells;
        this.champions = champions;
    }

    #region implemented abstract members of JsonDecodable
    
    public override void Init(JsonData j)
    {
        this.spells = new List<SpellCardData>();
        this.spells = j.GetListFromJsonData<SpellCardData>("spells") ?? new List<SpellCardData>();

        this.champions = new List<ChampionCardData>();
        this.champions = j.GetListFromJsonData<ChampionCardData>("champions");
    }
    
    #endregion

    #region public methods
    #endregion


	#region private functions
	#endregion
}

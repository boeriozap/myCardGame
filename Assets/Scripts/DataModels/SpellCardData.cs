﻿/*
 * SpellCardData
 * Copyright (c) Alessio Zap Boerio
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class SpellCardData : JsonDecodable
{
    [Header("General info")]
    public string title = "";
    public ClanColor clanColor = ClanColor.Green;

    [TextArea(2,3)]
    public string description = "";
    public string image_url = "";
    public int mana_cost = 0;
//    public CardRarity rarity;

//    [Header("Champion Info")]
//    public int maxToughness;
//    public int power;
//    public int movement = 2;
//    public int attackRange = 1;
//    public string CreatureScriptName;

//    [Header("Passive skill")]
//    public ChampionPassiveSkill passiveSkill;
//    public ChampionActiveSkill secondarySkill;
//    public ChampionActiveSkill mainSkill;

//    [Header("SpellInfo")]
//    public string spellScriptName;
//    public TargetingOptions targets;

    public SpellCardData()
    {
        this.title = "";
        this.clanColor = ClanColor.Green;
        this.description = "";
        this.image_url = "";
        this.mana_cost = 1;
    }

    public SpellCardData(string title, int clanColor, string description, string imageUrl, int manaCost)
    {
        this.title = title;
        this.clanColor = (ClanColor)clanColor;
        this.description = description;
        this.image_url = imageUrl;
        this.mana_cost = manaCost;
    }

    #region implemented abstract members of JsonDecodable

    public override void Init(JsonData j)
    {
        this.title = (string)j["title"];
        this.clanColor = (ClanColor)((int)j["clan_color"]);
        this.description = (string)j["description"];
        this.image_url = (string)j["image_url"];
        this.mana_cost = (int)j["mana_cost"];
    }

    #endregion
}

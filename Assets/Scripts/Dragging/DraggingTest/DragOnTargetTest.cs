﻿/*
 * DragOnTargetTest
 * Copyright (c) Alessio Zap Boerio
 */

using UnityEngine;
using System.Collections;
using DG.Tweening;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(LineRenderer))]
public class DragOnTargetTest : DraggingActionsTest
{
    public TargetingOptions targets = TargetingOptions.AllChampions;
    private SpriteRenderer _sr;
    private LineRenderer _lr;
    private Transform _triangle;
    private SpriteRenderer _triangleSR;
    private GameObject _target;

    void Awake()
    {
        _sr = GetComponent<SpriteRenderer>();
        _lr = GetComponentInChildren<LineRenderer>();
        _lr.sortingLayerName = "AboveEverything";
        _triangle = transform.Find("Triangle");
        _triangleSR = _triangle.GetComponent<SpriteRenderer>();
    }

    public override void OnStartDrag()
    {
        _sr.enabled = true;
        _lr.enabled = true;
    }

    public override void OnDraggingInUpdate()
    {
        // This code only draws the arrow
        Vector3 notNormalized = transform.position - transform.parent.position;
        Vector3 direction = notNormalized.normalized;
        float distanceToTarget = (direction*2.3f).magnitude;
        if (notNormalized.magnitude > distanceToTarget)
        {
            // draw a line between the creature and the target
            _lr.SetPositions(new Vector3[]{ transform.parent.position, transform.position - direction*2.3f });
            _lr.enabled = true;

            // position the end of the arrow between near the target.
            _triangleSR.enabled = true;
            _triangleSR.transform.position = transform.position - 1.5f*direction;

            // proper rotarion of arrow end
            float rot_z = Mathf.Atan2(notNormalized.y, notNormalized.x) * Mathf.Rad2Deg;
            _triangleSR.transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
        }
        else
        {
            // if the target is not far enough from creature, do not show the arrow
            _lr.enabled = false;
            _triangleSR.enabled = false;
        }

    }

    public override void OnEndDrag()
    {

        // return target and arrow to original position
        // this position is special for spell cards to show the arrow on top
        transform.localPosition = new Vector3(0f, 0f, 0.4f);
        _sr.enabled = false;
        _lr.enabled = false;
        _triangleSR.enabled = false;

    }

    // NOT USED IN THIS SCRIPT
    protected override bool DragSuccessful()
    {
        return true;
    }
}

﻿/*
 * DraggableTestWithActions
 * Copyright (c) Alessio Zap Boerio
 */

using UnityEngine;
using System.Collections;

public class DraggableTestWithActions : MonoBehaviour {

    public bool usePointerDisplacement = true;
    // PRIVATE FIELDS
    // a reference to a DraggingActionsTest script
    private DraggingActionsTest _draggingAction;

    // a flag to know if we are currently dragging this GameObject
    private bool _isDragging = false;

    // distance from the center of this Game Object to the point where we clicked to start dragging 
    private Vector3 _pointerDisplacement = Vector3.zero;

    // distance from camera to mouse on Z axis 
    private float _zDisplacement;

    void Awake()
    {
        _draggingAction = GetComponent<DraggingActionsTest>();
    }

    void OnMouseDown()
    {
        if (_draggingAction.CanDrag)
        {
            _isDragging = true;
            _draggingAction.OnStartDrag();
            _zDisplacement = -Camera.main.transform.position.z + transform.position.z;
            if (usePointerDisplacement)
                _pointerDisplacement = -transform.position + MouseInWorldCoords();
            else
                _pointerDisplacement = Vector3.zero;
        }
    }

    void Update ()
    {
        if (_isDragging)
        { 
            Vector3 mousePos = MouseInWorldCoords();
            _draggingAction.OnDraggingInUpdate();
            //Debug.Log(mousePos);
            transform.position = new Vector3(mousePos.x - _pointerDisplacement.x, mousePos.y - _pointerDisplacement.y, transform.position.z);   
        }
    }

    void OnMouseUp()
    {
        if (_isDragging)
        {
            _isDragging = false;
            _draggingAction.OnEndDrag();
        }
    }   

    // returns mouse position in World coordinates for our GameObject to follow. 
    private Vector3 MouseInWorldCoords()
    {
        var screenMousePos = Input.mousePosition;
        //Debug.Log(screenMousePos);
        screenMousePos.z = _zDisplacement;
        return Camera.main.ScreenToWorldPoint(screenMousePos);
    }
}

﻿/*
 * GameManager
 * Copyright (c) Alessio Zap Boerio
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public BoardManager boardManager;

    public void TryLogin()
    {
        var userService = new UserAPIService();
        userService.FBLogin((resource) => {
            print("FBLogin:");
            if (resource.error != null)
            {
                print(resource.error);
            }
            else
            {
                var facebookToken = resource.resource;
                userService.Login(facebookToken: facebookToken, callback: (sessionTokenResource) => {
                    if (resource.error != null)
                    {
                        print(sessionTokenResource.error);
                    }
                    else
                    {
                        userService.GetToken((tokenResource) => {
                            if (tokenResource.error != null)
                            {
                                print(tokenResource.error);
                            }
                            else
                            {
                                print("token: " + tokenResource.resource);
                            }
                        });
                    }
                });
            }
        });
    }
	
}

﻿/*
 * Map
 * Copyright (c) Alessio Zap Boerio
 */

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;

public interface IMapDelegate
{
    void MapCellDidTap(MapCell mapCell);
    void OutsideOfMapDidTap();
}

public class Map : MonoBehaviour, IMapCellDelegate, IMapBattlegroundDelegate
{
    public IMapDelegate mapDelegate = null;

    public MapCell[] mapCells;
    public int neighbursDepth = 1;

    [SerializeField]
    private MapBattleground _mapBattleground;
    [SerializeField]
    private GameObject _mapCellPrefab;
    [SerializeField]
    private Color _availableCellsColor;
    [SerializeField]
    private Color _unavailableCellsColor;
    private List<MapCell> _selectedCells = new List<MapCell>();

    #region public methods

    public void ConfigureMap()
    {
        mapCells = transform.GetComponentsInChildren<MapCell>();
        foreach(MapCell cell in mapCells)
        {
            cell.Configure(mapCellDelegate: this);
        }

        _mapBattleground.battlegroundDelegate = this;
    }

    public List<MapCell> GetNeighboursOf(MapCell targetCell, int depth)
    {
        if(depth < 0)
            depth = 0;
        
        HashSet<MapCell> neighbours = new HashSet<MapCell>();
        neighbours.Add(targetCell);

        while(depth > 0)
        {
            HashSet<MapCell> neighboursCopy = new HashSet<MapCell>(neighbours);
            foreach(MapCell cell in neighboursCopy)
            {
                List<MapCell> localNeighbours = cell.GetNeighbours();
                foreach(MapCell neighbour in localNeighbours)
                {
                    neighbours.Add(neighbour);
                }
            }
            depth--;
        }

        return neighbours.ToList<MapCell>();
    }

    public void SelectCells()
    {
        foreach(MapCell cell in this._selectedCells)
        {
            if(cell.canMoveIn)
            {
                cell.Highlight(this._unavailableCellsColor);
            } else {
                cell.Highlight(this._availableCellsColor);
            }
        }
    }

    public void DeselectCells()
    {
        foreach(MapCell cell in this._selectedCells)
            cell.StopHighlight();
        this._selectedCells = new List<MapCell>();
    }

    #endregion

    #region IMapCellDelegate implementation

    public void MapCellDidTap(MapCell mapCell)
    {
        this.mapDelegate.MapCellDidTap(mapCell);
//        DeselectCells();
//
//        List<MapCell> neighbours = GetNeighboursOf(targetCell: mapCell, depth: this.neighbursDepth);
//
//        this._selectedCells.AddRange(neighbours);
//        SelectCells();
    }

    #endregion

    #region IMapBattlegroundDelegate implementation

    public void BattlegroundDidTap()
    {
        mapDelegate.OutsideOfMapDidTap();
    }

    #endregion

    #region private functions

    #endregion
}


﻿/*
 * MapBattleground
 * Copyright (c) Alessio Zap Boerio
 */

using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


public interface IMapBattlegroundDelegate 
{
    void BattlegroundDidTap();
}

[RequireComponent(typeof(Renderer))]
public class MapBattleground : MonoBehaviour, IPointerClickHandler
{
    public IMapBattlegroundDelegate battlegroundDelegate;

    void Awake()
    {
        this.gameObject.GetComponent<Renderer>().sortingLayerName = SortingLayerType.Battleground.ToString();
    }

    #region IPointerClickHandler implementation

    public void OnPointerClick(PointerEventData eventData)
    {
        if(battlegroundDelegate != null)
        {
            battlegroundDelegate.BattlegroundDidTap();
        }
    }

    #endregion
}


﻿/*
 * MapCell
 * Copyright (c) Alessio Zap Boerio
 */

using UnityEngine;
using System.Collections.Generic;
using System.Security.AccessControl;
using UnityEngine.EventSystems;

// TODO: remove from here
[System.Serializable]
public class Champion
{
    
}

public interface IMapCellDelegate
{
    void MapCellDidTap(MapCell mapCell);
}

public enum MapCellType: int {
    Null,
    Spawn,
    Generic,
    Home
}

[RequireComponent(typeof(Renderer))]
public class MapCell : MonoBehaviour, IPointerClickHandler
{
    public MapCellType type = MapCellType.Generic;
    public IMapCellDelegate mapCellDelegate = null;

    public bool canMoveIn
    {
        get { return champion != null && type != MapCellType.Home; }
    }
    public bool canBeAttacked
    {
        // TODO: aggiungere il controllo currentPlayerId != _champion.playerOwnerId
        get { return type == MapCellType.Home || champion != null /*&& type != MapCellType.Home*/; }
    }

    public ChampionCardViewModel champion;
    [SerializeField]
    private List<MapCell> _neighbours;

    private Transform _transform;
    private Renderer _renderer;
    private Color _defaultColor;

    void Awake()
    {
        _transform = this.transform;
        _renderer = this.gameObject.GetComponent<Renderer>();
        _renderer.sortingLayerName = SortingLayerType.Cell.ToString();
    }

    void Start()
    {
        CheckChampion();
    }

    #region IPointerClickHandler implementation

    public void OnPointerClick(PointerEventData eventData)
    {
        if(mapCellDelegate != null)
        {
            mapCellDelegate.MapCellDidTap(this);
        }
    }

    #endregion

    #region public methods

    public void Configure(IMapCellDelegate mapCellDelegate)
    {
        this.mapCellDelegate = mapCellDelegate;

        Vector3 position = _transform.localPosition;
        position = GetIntPosition();
        this.gameObject.name = "MapCell[" + position.x + ";" + position.y + "]";
        _transform.localPosition = position;
        
        CheckChampion();
        SetImage();
    }

    public void Highlight(Color highlightColor)
    {
        this._defaultColor = this._renderer.material.color;
        this._renderer.material.color = highlightColor;
    }

    public void StopHighlight()
    {
        this._renderer.material.color = this._defaultColor;
    }

    public List<MapCell> GetNeighbours()
    {
        return _neighbours;
    }

    #endregion

    #region private functions

    private Vector3 GetIntPosition()
    {
        int x = Mathf.RoundToInt(_transform.localPosition.x);
        int y = Mathf.RoundToInt(_transform.localPosition.y);
        int z = 0;
        return new Vector3(x,y,z);
    }

    private void SetImage()
    {
        Material cellMaterial = Resources.Load<Material>("Materials/MapCell_" + this.type.ToString());
        switch (this.type)
        {
            case MapCellType.Null:
                break;
            case MapCellType.Spawn:
                _renderer.material = cellMaterial;
                break;
            case MapCellType.Generic:
                _renderer.material = cellMaterial;
                break;
            case MapCellType.Home:
                break;
        }
    }

    private void CheckChampion()
    {
        champion = this.GetComponentInChildren<ChampionCardViewModel>();
        if (champion != null)
        {
            champion.currentCell = this;
        }
    }

    #endregion
}


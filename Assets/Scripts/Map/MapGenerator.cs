﻿/*
 * MapGenerator
 * Copyright (c) Alessio Zap Boerio
 */

using UnityEngine;
using System.Collections;

public class MapGenerator
{
    private MapCellType[,] _firstMap = new MapCellType[,]
    {
        { MapCellType.Null,     MapCellType.Spawn,      MapCellType.Spawn,      MapCellType.Spawn,      MapCellType.Spawn,      MapCellType.Spawn,      MapCellType.Spawn,      MapCellType.Null },
        { MapCellType.Generic,  MapCellType.Generic,    MapCellType.Generic,    MapCellType.Generic,    MapCellType.Generic,    MapCellType.Generic,    MapCellType.Generic,    MapCellType.Generic },
        { MapCellType.Generic,  MapCellType.Generic,    MapCellType.Null,       MapCellType.Generic,    MapCellType.Generic,    MapCellType.Null,       MapCellType.Generic,    MapCellType.Generic },
        { MapCellType.Generic,  MapCellType.Generic,    MapCellType.Null,       MapCellType.Generic,    MapCellType.Generic,    MapCellType.Null,       MapCellType.Generic,    MapCellType.Generic },

        { MapCellType.Generic,  MapCellType.Generic,    MapCellType.Null,       MapCellType.Generic,    MapCellType.Generic,    MapCellType.Null,       MapCellType.Generic,    MapCellType.Generic },
        { MapCellType.Generic,  MapCellType.Generic,    MapCellType.Null,       MapCellType.Generic,    MapCellType.Generic,    MapCellType.Null,       MapCellType.Generic,    MapCellType.Generic },
        { MapCellType.Generic,  MapCellType.Generic,    MapCellType.Generic,    MapCellType.Generic,    MapCellType.Generic,    MapCellType.Generic,    MapCellType.Generic,    MapCellType.Generic },
        { MapCellType.Null,     MapCellType.Spawn,      MapCellType.Spawn,      MapCellType.Spawn,      MapCellType.Spawn,      MapCellType.Spawn,      MapCellType.Spawn,      MapCellType.Null }
    };

    #region public method

    public MapConfiguration GetFirstMap() {
        
        return new MapConfiguration(_firstMap);
    }

    #endregion
}

public class MapConfiguration
{
    public MapCellType[,] normalCells;

    public MapConfiguration(MapCellType[,] normalCells)
    {
        this.normalCells = normalCells;
    }
}
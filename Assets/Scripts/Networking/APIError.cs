﻿/*
 * ResponseSerializer
 * Copyright (c) Alessio Zap Boerio
 */
using LitJson;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// All the handled kind of error the server can send us
/// </summary>
public enum APIErrorType
{
    // unknown kind of error
    Unknown,

    // error is from server
    BadRequest                  = 400,
    Unauthorized                = 401,
    NotFound                    = 404,

    // error is before server connection
    UnaxpectedError,
    Aborted,
    ConnectionTimedOut,
    TimedOut
}

/// <summary>
/// This object wraps all the information about the error, like the errorType, the description or the missingParameters
/// </summary>
public class APIError : JsonDecodable
{
    public APIErrorType errorType;

    public string description;

    public List<string> missingParameters;

    public APIError()
    {
        this.errorType = APIErrorType.Unknown;
        this.description = null;
        this.missingParameters = null;
    }

    public APIError(APIErrorType errorType, string description, string[] missingParameters = null)
    {
        this.errorType = errorType;
        this.description = description;
        this.missingParameters = null;
    }

    public override string ToString()
    {
        return string.Format("[APIError] [{0}] - {1}", this.errorType, this.description);
    }

    #region implemented abstract members of JsonDecodable
    public override void Init(JsonData j)
    {
        if(j.Keys.Contains("message"))
        {
            this.description = (string)j["message"];
        }

        if (j.Keys.Contains("data") && j["data"].IsArray)
        {
            this.missingParameters = new List<string>();
            for(int i=0; i<j["data"].Count; i++)
            {
                this.missingParameters.Add((string)j["data"][i]);
            }
        }
    }
    #endregion
}


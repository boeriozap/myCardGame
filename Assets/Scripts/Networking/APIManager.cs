﻿/*
 * APIManager
 * Copyright (c) Alessio Zap Boerio
 */

using System;
using BestHTTP;
using UnityEngine;
using LitJson;
using BestHTTP.SocketIO.JsonEncoders;
using System.Collections;
using System.Collections.Generic;


/// <summary>
/// This object contains the aspected resource of type T if the error is null.
/// </summary>
public class APIResource<T>
{
    /// <summary>
    /// The error of the response. Always check this before all: if is null, use the 'resource' obj
    /// </summary>
    public APIError error;
    /// <summary>
    /// The resource found. Always check the error before all: if the error is null, you'll find a correct T resource here
    /// </summary>
    public T resource;

    public APIResource()
    {
        error = null;
    }
}
    

/// <summary>
/// The API manager.
/// </summary>
public class APIManager
{
    /// <summary>
    /// The base URL.
    /// </summary>
    public static string BaseUrl = "http://localhost:8080";

    /// <summary>
    /// This is the stored refresh token. It could be an empty string if no refresh token has been stored before.
    /// It is the prove that the user is logged. We need to store this token to get a temporarily one (SessionToken).
    /// When the user will logout, we will delete this token
    /// </summary>
    /// <value>The refresh token.</value>
    public static string RefreshToken {
        get {
            if(PlayerPrefs.HasKey("RefreshToken"))
                return PlayerPrefs.GetString("RefreshToken");
            return "";
        }
    }

    /// <summary>
    /// The temporarily session token to add to each API call. A 401 status code means an invalid SessionToken.
    /// To get a new SessionToken, send the RefreshToken with UserAPIService.GetToken(RefreshToken)
    /// </summary>
    public static string SessionToken = "";

    /// <summary>
    /// Saves the refresh token.
    /// </summary>
    /// <param name="token">Token.</param>
    public static void SaveRefreshToken(string token)
    {
        PlayerPrefs.SetString("RefreshToken", token);
        PlayerPrefs.Save();
    }

    /// <summary>
    /// Clears the refresh token.
    /// </summary>
    public static void ClearRefreshToken()
    {
        if(PlayerPrefs.HasKey("RefreshToken"))
        {
            PlayerPrefs.DeleteKey("RefreshToken");
            PlayerPrefs.Save();
        }
    }

    /// <summary>
    /// This Method creates an HTTPRequest with a preconstructed parameters and callback.
    /// Its role is manage the response and pass it to the Serializer to create the desired object.
    /// </summary>
    /// <param name="uri">URI.</param>
    /// <param name="methodType">Method type.</param>
    /// <param name="callback">Callback. Define here how to manage the APIResource or the error</param>
    public static HTTPRequest Request(Uri uri, HTTPMethods methodType, Action<APIError,HTTPResponse> callback ) {
        HTTPRequest httpRequest = new HTTPRequest(uri, methodType, (request, response) =>
            {
                APIError error = null;

                switch (request.State)
                {
                    // The request finished without any problem.
                    case HTTPRequestStates.Finished:
                        // Manage here the status code

                        // Bad Request, error on syntax
                        if      (400 == response.StatusCode){
                            error = ResponseSerializer.SerializeError(response: response);
                            error.errorType = APIErrorType.BadRequest;
                            if (!string.IsNullOrEmpty(error.description))
                                error.description = response.Message;
                        }
                        // Unauthorized, wrong or expired SessionToken
                        else if (401 == response.StatusCode){
                            error = ResponseSerializer.SerializeError(response: response);
                            error.errorType = APIErrorType.Unauthorized;
                            if (!string.IsNullOrEmpty(error.description))
                                error.description = response.Message;
                        }
                        // Not Found, the asked resource doesn't exist
                        else if (404 == response.StatusCode){
                            error = ResponseSerializer.SerializeError(response: response);
                            error.errorType = APIErrorType.NotFound;
                            if (!string.IsNullOrEmpty(error.description))
                                error.description = response.Message;
                        }
                        // Generic error
                        else if (402 <= response.StatusCode && response.StatusCode <= 499) {
                            error = ResponseSerializer.SerializeError(response: response);
                            error.errorType = APIErrorType.Unknown;
                            if (!string.IsNullOrEmpty(error.description))
                                error.description = response.Message;
                        }
                        // Success
                        else {
                            
                        }
                        break;

                        // The request finished with an unexpected error.
                        // The request's Exception property may contain more information about the error.
                    case HTTPRequestStates.Error:
                        error = new APIError();
                        error.errorType = APIErrorType.UnaxpectedError;
                        error.description = "Request Finished with Error! " +
                            (request.Exception != null ?
                                (request.Exception.Message + "\n" + request.Exception.StackTrace) :
                                "No Exception");
                        break;

                        // The request aborted, initiated by the user.
                    case HTTPRequestStates.Aborted:
                        error = new APIError();
                        error.errorType = APIErrorType.Aborted;
                        error.description = "Request Aborted!";
                        break;

                        // Ceonnecting to the server timed out.
                    case HTTPRequestStates.ConnectionTimedOut:
                        error = new APIError();
                        error.errorType = APIErrorType.ConnectionTimedOut;
                        error.description = "Connection Timed Out!";
                        break;

                        // The request didn't finished in the given time.
                    case HTTPRequestStates.TimedOut:
                        error = new APIError();
                        error.errorType = APIErrorType.TimedOut;
                        error.description = "Processing the request Timed Out!";
                        break;
                }
                callback(error, response);
            });
        
        httpRequest.Timeout = TimeSpan.FromSeconds(10);
        httpRequest.DisableCache = true;
        return httpRequest;
    }
}


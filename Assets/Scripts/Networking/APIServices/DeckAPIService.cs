﻿using System;
using BestHTTP;
using System.Collections.Generic;


public class DeckAPIService
{
    private static string GetDeckEndpoint = "/deck";
    private static string GetDecksEndpoint = "/decks";

    public void GetDeck(Action<APIResource<DeckData>> callback) {
        string uriString = APIManager.BaseUrl + GetDeckEndpoint;
        uriString = "http://www.mocky.io/v2/59ca77232d0000f200806885";
        Uri uri = new Uri(uriString);
        var request = APIManager.Request(uri, HTTPMethods.Get, (error, response) => {
            ResponseSerializer.SerializeResponseObject<DeckData>(error, response, (resource) => {
                callback(resource);
            });
        });
        request.Send();
    }


    public void GetDecks(Action<APIResource<List<DeckData>>> callback) {
        string uriString = APIManager.BaseUrl + GetDecksEndpoint;
        uriString = "http://www.mocky.io/v2/59ca77472d0000bc00806887";
        Uri uri = new Uri(uriString);
        var request = APIManager.Request(uri, HTTPMethods.Get, (error, response) => {
            ResponseSerializer.SerializeResponseCollection<DeckData>(error, response, (resource) => {
                callback(resource);
            });
        });
        request.Send();
    }
}


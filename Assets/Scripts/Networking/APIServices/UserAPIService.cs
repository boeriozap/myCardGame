﻿/*
 * UserAPIService
 * Copyright (c) Alessio Zap Boerio
 */

using System;
using BestHTTP;
using System.Collections.Generic;
using Facebook.Unity;
using LitJson;


/// <summary>
/// This API Service makes call about objects of type User
/// </summary>
public class UserAPIService
{
    private static string FacebookLoginEndpoint = "/auth/facebook/login";
    private static string GetTokenEndpoint = "/auth/token";

    /// <summary>
    /// Calls the facebook sdk login
    /// </summary>
    /// <param name="callback">Callback.</param>
    public void FBLogin(Action<APIResource<string>> callback) {
        if(!FB.IsInitialized) {
            FB.Init(
                onInitComplete: () => {
                FB.ActivateApp();
                FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, (result) => {
                    callback(HandleFBLoginResult(result));
                });
            },
                onHideUnity: null,
                authResponse: null);
        }
        else
        {
            FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, (result) => {
                callback(HandleFBLoginResult(result));
            });
        }
    }

    /// <summary>
    /// Try to login with facebook token.
    /// </summary>
    /// <param name="facebookToken">Facebook token.</param>
    /// <param name="callback">Callback.</param>
    public void Login(string facebookToken, Action<APIResource<string>> callback) {
        string uriString = APIManager.BaseUrl + FacebookLoginEndpoint;
        Uri uri = new Uri(uriString);
        var request = APIManager.Request(uri, HTTPMethods.Post, (error, response) => {
            // TODO: allegare in json il facebook token
            ResponseSerializer.SerializeResponse(error, response, "sessionToken", (resource) => {
                if(resource.error != null)
                {
                    // nothing to do at the moment
                }
                else 
                {
                    APIManager.SaveRefreshToken(resource.resource);
                }
                callback(resource);
            });
        });

        Dictionary<string, object> parameters = new Dictionary<string, object>();
        parameters.Add("facebookToken", facebookToken);
        var json = JsonMapper.ToJson(parameters);

        request.AddHeader("Content-Type", "application/json");
        request.RawData = System.Text.Encoding.UTF8.GetBytes(json);

        request.Send();
    }

    /// <summary>
    /// Gets the temporarily token to do requests to server. You need to have a valid APIManager.RefreshToken to get a valid accessToken.
    /// </summary>
    /// <param name="callback">Callback.</param>
    public void GetToken(Action<APIResource<string>> callback) {
        string uriString = APIManager.BaseUrl + GetTokenEndpoint;
        uriString += "?sessionToken=" + APIManager.RefreshToken;
        Uri uri = new Uri(uriString);
        var request = APIManager.Request(uri, HTTPMethods.Get, (error, response) => {
            ResponseSerializer.SerializeResponse(error, response, "accessToken",(resource) => {
                if(resource.error != null)
                {
                    // nothing to do at the moment
                }
                else 
                {
                    APIManager.SessionToken = resource.resource; 
                }
                callback(resource);
            });
        });
        request.Send();
    }

    #region private functions

    /// <summary>
    /// Handles the FB login result and try to return an APIResource with a valid AccessToken inside it.
    /// </summary>
    /// <returns>The FB login result.</returns>
    /// <param name="result">Result.</param>
    protected APIResource<string> HandleFBLoginResult(IResult result)
    {
        var resource = new APIResource<String>();
        resource.resource = null;

        if (result == null)
        {
            resource.error = new APIError(errorType: APIErrorType.Unknown, description: "result is null");
        }

        // Some platforms return the empty string instead of null.
        if (!string.IsNullOrEmpty(result.Error))
        {
            resource.error  = new APIError(errorType: APIErrorType.Unknown, description: result.Error);
        }
        else if (result.Cancelled)
        {
            resource.error  = new APIError(errorType: APIErrorType.Aborted, description: result.RawResult);
        }
        else if (!string.IsNullOrEmpty(result.RawResult))
        {
            var resultDic = result.ResultDictionary ?? new Dictionary<string, object>();
            var permissionsValue = resultDic.ContainsKey("permissions") ? (string)resultDic["permissions"] : "";
            var permissions = permissionsValue.Split(',');
            var accessToken = resultDic.ContainsKey("access_token") ? (string)resultDic["access_token"] : "";
            var userId = resultDic.ContainsKey("user_id") ? (string)resultDic["user_id"] : "";
            if(!string.IsNullOrEmpty(accessToken))
            {
                var facebookLoginObject = new FacebookLoginObject(permissions, accessToken, userId);
                resource.resource = facebookLoginObject.accessToken;
            }
            else
            {
                resource.error  = new APIError(errorType: APIErrorType.NotFound, description: "No Access Token found");
            }
        }
        else
        {
            resource.error  = new APIError(errorType: APIErrorType.Unknown, description: "Empty Response\n");
        }

        return resource;
    }

    #endregion

    /// <summary>
    /// This object is to wrap the facebook login response
    /// </summary>
    class FacebookLoginObject
    {
        public string[] permissions;
        public string accessToken;
        public string userId;

        public FacebookLoginObject(string[] permissions, string accessToken, string userId)
        {
            this.permissions = permissions;
            this.accessToken = accessToken;
            this.userId = userId;
        }
    }
}


﻿/*
 * ResponseSerializer
 * Copyright (c) Alessio Zap Boerio
 */

using System;
using LitJson;
using BestHTTP;
using System.Collections.Generic;

/// <summary>
/// Json decodable objects have to implement the Init(JsonData) method.
/// </summary>
public abstract class JsonDecodable 
{
    public abstract void Init(JsonData j);
    public JsonDecodable(){}
}

/// <summary>
/// This class has useful static methods to serialize HTTPResponse into objects.
/// </summary>
public class ResponseSerializer
{
    /// <summary>
    /// Serializes the response into object of type T:JsonDecodable.
    /// </summary>
    /// <param name="error">Error.</param>
    /// <param name="response">Response.</param>
    /// <param name="callback">Callback.</param>
    /// <typeparam name="T">The 1st type parameter, it should be a JsonDecodable.</typeparam>
    public static void SerializeResponseObject<T>(APIError error, HTTPResponse response, Action<APIResource<T>> callback)
        where T: JsonDecodable, new()
    {
        var resource = new APIResource<T>();

        if (error != null)
        {
            resource.error = error;
            resource.resource = null;
        }
        else
        {
            resource.error = null;
            JsonData jsonData = JsonMapper.ToObject(response.DataAsText);
            T newObj = new T();
            newObj.Init(jsonData);
            resource.resource = newObj;
        }
        callback(resource);
    }

    /// <summary>
    /// Serializes the response into collection of T:JsonDecodable element type.
    /// </summary>
    /// <param name="error">Error.</param>
    /// <param name="response">Response.</param>
    /// <param name="callback">Callback.</param>
    /// <typeparam name="T">The 1st type parameter, it should be a JsonDecodable.</typeparam>
    public static void SerializeResponseCollection<T>(APIError error, HTTPResponse response, Action<APIResource<List<T>>> callback)
        where T: JsonDecodable, new()
    {
        var resource = new APIResource<List<T>>();
        List<T> collection = new List<T>();

        if (error != null)
        {
            resource.error = error;
            resource.resource = null;
        }
        else
        { 
            resource.error = null;
            JsonData jsonData = JsonMapper.ToObject(response.DataAsText);
            for(int i=0; i<jsonData.Count; i++)
            {
                T newObj = new T();
                newObj.Init(jsonData[i]);
                collection.Add(newObj);
            }
            resource.resource = collection;
        }
    }

    /// <summary>
    /// Serializes the response into simple object (if the wanted object type is string, int, bool).
    /// If you want an advanced or more complex object, use the SerializeResponseObject<T> or SerializeResponseCollection<T>.
    /// </summary>
    /// <param name="error">Error.</param>
    /// <param name="response">Response.</param>
    /// <param name="insideKey">insideKey.</param>
    /// <param name="callback">Callback.</param>
    #region SerializeResponse of type string, int, bool

    /// <summary>
    /// Serializes the response into simple object if the wanted object type is String, Int, Bool, Float.
    /// If you want an advanced or complex object, use the SerializeResponseObject<T> or SerializeResponseCollection<T>.
    /// </summary>
    /// <param name="error">Error.</param>
    /// <param name="response">Response.</param>
    /// <param name="insideKey">insideKey.</param>
    /// <param name="callback">Callback.</param>
    public static void SerializeResponse(APIError error, HTTPResponse response, string insideKey, Action<APIResource<string>> callback)
    {
        var resource = new APIResource<string>();

        if (error != null)
        {
            resource.error = error;
            resource.resource = null;
        }
        else
        {
            JsonData jsonData = JsonMapper.ToObject(response.DataAsText);
            if(!jsonData.Keys.Contains(insideKey))
            {
                var newError = ResponseSerializer.SerializeError(response: response);
                newError.errorType = APIErrorType.NotFound;
                if (!string.IsNullOrEmpty(error.description))
                    newError.description = "No " + insideKey + " key inside the response";

                resource.error = newError;
                resource.resource = null;
            }
            else
            {
                resource.error = null;
                resource.resource = (string)jsonData[insideKey];
            }
        }
        callback(resource);
    }

    /// <summary>
    /// Serializes the response into simple object if the wanted object type is String, Int, Bool, Float.
    /// If you want an advanced or complex object, use the SerializeResponseObject<T> or SerializeResponseCollection<T>.
    /// </summary>
    /// <param name="error">Error.</param>
    /// <param name="response">Response.</param>
    /// <param name="insideKey">insideKey.</param>
    /// <param name="callback">Callback.</param>
    public static void SerializeResponse(APIError error, HTTPResponse response, string insideKey, Action<APIResource<int>> callback)
    {
        var resource = new APIResource<int>();

        if (error != null)
        {
            resource.error = error;
        }
        else
        {
            resource.error = null;
            JsonData jsonData = JsonMapper.ToObject(response.DataAsText);
            resource.resource = (int)jsonData[insideKey];
        }
        callback(resource);
    }

    /// <summary>
    /// Serializes the response into simple object if the wanted object type is String, Int, Bool, Float.
    /// If you want an advanced or complex object, use the SerializeResponseObject<T> or SerializeResponseCollection<T>.
    /// </summary>
    /// <param name="error">Error.</param>
    /// <param name="response">Response.</param>
    /// <param name="insideKey">insideKey.</param>
    /// <param name="callback">Callback.</param>
    public static void SerializeResponse(APIError error, HTTPResponse response, string insideKey, Action<APIResource<bool>> callback)
    {
        var resource = new APIResource<bool>();

        if (error != null)
        {
            resource.error = error;
        }
        else
        {
            resource.error = null;
            JsonData jsonData = JsonMapper.ToObject(response.DataAsText);
            resource.resource = (bool)jsonData[insideKey];
        }
        callback(resource);
    }

    #endregion

    public static APIError SerializeError(HTTPResponse response)
    {
        JsonData jsonData = JsonMapper.ToObject(response.DataAsText);
        var error = new APIError();
        error.Init(jsonData);

        return error;
    }
}

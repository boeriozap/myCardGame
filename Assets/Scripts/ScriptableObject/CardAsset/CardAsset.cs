﻿/*
 * CardAsset
 * Copyright (c) Alessio Zap Boerio
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CardAsset : ScriptableObject 
{
    [Header("General info")]
    public ClanColor clanColor;
    public CharacterAsset championAsset;

    [TextArea(2,3)]
    public string description;
    public Sprite cardImage;
    public int manaCost;
    public CardRarity rarity;

    [Header("Champion Info")]
    public int maxToughness;
    public int power;
    public int movement = 1;
    public int attackRange = 1;
//    public bool Taunt;
//    public bool Charge;
    public string CreatureScriptName;
//    public int specialCreatureAmount;

    [Header("Passive skill")]
    public ChampionPassiveSkill passiveSkill;
    public ChampionActiveSkill secondarySkill;
    public ChampionActiveSkill mainSkill;

    [Header("SpellInfo")]
    public string spellScriptName;
    public TargetingOptions targets;

}

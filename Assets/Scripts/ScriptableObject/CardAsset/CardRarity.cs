﻿/*
 * CardRarity
 * Copyright (c) Alessio Zap Boerio
 */

public enum CardRarity
{
    Common,
    Uncommon,
    Rare
}

﻿/*
 * ChampionSkill
 * Copyright (c) Alessio Zap Boerio
 */

[System.Serializable]
public abstract class ChampionSkill
{
    public string description;
    public TargetingOptions targets;
}

[System.Serializable]
public class ChampionActiveSkill: ChampionSkill
{
    public int manaCost;
}

[System.Serializable]
public class ChampionPassiveSkill: ChampionSkill
{

}

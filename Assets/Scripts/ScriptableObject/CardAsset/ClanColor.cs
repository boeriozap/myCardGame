﻿/*
 * ClanColor
 * Copyright (c) Alessio Zap Boerio
 */

public enum ClanColor
{
    Green = 0,
    Red,
    Purple,
    White,
    Black
}

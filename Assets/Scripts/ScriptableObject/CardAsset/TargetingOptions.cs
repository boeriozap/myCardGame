﻿/*
 * TargetingOptions
 * Copyright (c) Alessio Zap Boerio
 */

public enum TargetingOptions
{
    NoTarget,
    AllPermanent,
    SinglePermanent,
    AllChampions, 
    EnemyChampions,
    YourChampions, 
    SingleChampion, 
    EnemySingleChampion,
    YourSingleChampion,
    Enemy,
    You
}

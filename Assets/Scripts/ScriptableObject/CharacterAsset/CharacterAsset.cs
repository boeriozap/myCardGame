﻿/*
 * CharacterAsset
 * Copyright (c) Alessio Zap Boerio
 */

using UnityEngine;
using System.Collections;

public class CharacterAsset : ScriptableObject 
{
    public ClanColor clanColor;
    public int level = 1;
    public int power = 1;
    public int toughness = 1;
    public int attackRange = 1;
    public string HeroPowerName;
    public Sprite AvatarImage;
    public Sprite HeroPowerIconImage;
    public Sprite AvatarBGImage;
    public Sprite HeroPowerBGImage;
    public Color32 AvatarBGTint;
    public Color32 HeroPowerBGTint;
}

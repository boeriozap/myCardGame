﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script should be attach to the card gameobject to see correct rotation
/// </summary>

[ExecuteInEditMode]
public class BetterCardRotation : MonoBehaviour
{
	// parent gameobject for all the card face graphics
	public RectTransform cardFront;

	// parent gameobject for all the card back graphics
	public RectTransform cardBack;

	// an empty gameobject that it must be placed a little bit above the card, in the center
	public Transform targetFacePoint;

	// the collider of the card
	public Collider cardCollider;

	// if true, the player can see the back of the card
	private bool _showingBack = false;

	void Update()
	{
		RaycastHit[] hits;
		hits = Physics.RaycastAll(
			origin: Camera.main.transform.position,
			direction: (-Camera.main.transform.position + targetFacePoint.position).normalized,
			maxDistance: (-Camera.main.transform.position + targetFacePoint.position).magnitude
		);

		bool passedThroughColliderOnCard = false;

		foreach(RaycastHit h in hits)
		{
			if(h.collider == this.cardCollider)
				passedThroughColliderOnCard = true;
		}

		if(passedThroughColliderOnCard != this._showingBack)
		{
			_showingBack = passedThroughColliderOnCard;
			this.cardFront.gameObject.SetActive(!_showingBack);
			this.cardBack.gameObject.SetActive(_showingBack);
		}
	}
}

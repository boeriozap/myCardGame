﻿/*
 * HoverPreview
 * Copyright (c) Alessio Zap Boerio
 */

using UnityEngine;
using System.Collections;
using DG.Tweening;

public class HoverPreview: MonoBehaviour
{
    public GameObject turnThisOffWhenPreviewing;  // if this is null, will not turn off anything 
    public Vector3 targetPosition;
    public float targetScale;
    public GameObject previewGameObject;
    public bool activateInAwake = false;
    [Range(0f,1f)]
    public float animationTime = 0f;

    private static HoverPreview _currentPreview = null;

    private ChampionCardViewModel _championCardViewModel = null;
    public bool IsChampionOnTheField
    {
        get { return _championCardViewModel != null; }
    }

    private static bool _arePreviewsAllowed = true;
    public static bool ArePreviewsAllowed
    {
        get { return _arePreviewsAllowed;}
        set 
        { 
            _arePreviewsAllowed= value;
            if (!_arePreviewsAllowed)
                StopAllPreviews();
        }
    }

    private bool _isThisPreviewEnabled = false;
    public bool IsThisPreviewEnabled
    {
        get { return _isThisPreviewEnabled;}
        set 
        { 
            _isThisPreviewEnabled = value;
            if (!_isThisPreviewEnabled)
                StopThisPreview();
        }
    }

    public bool IsOveringCollider { get; set; }
 
    #region Monobehaviour
    void Awake()
    {
        IsThisPreviewEnabled = activateInAwake;
    }

    void Start()
    {
        _championCardViewModel = this.gameObject.GetComponent<ChampionCardViewModel>();
    }
            
    void OnMouseEnter()
    {
        IsOveringCollider = true;
        if (ArePreviewsAllowed && IsThisPreviewEnabled)
            PreviewThisObject();
    }
        
    void OnMouseExit()
    {
        IsOveringCollider = false;
        if (!IsPreviewingSomeCard())
            StopAllPreviews();
    }
    #endregion

    #region private functions
    void PreviewThisObject()
    {
        // 1) first disable the previous preview if there is one already
        StopAllPreviews();
        // 2) save this HoverPreview as curent
        _currentPreview = this;
        // 3) enable Preview game object
        previewGameObject.SetActive(true);
        // 4) disable if we have what to disable
        if (turnThisOffWhenPreviewing!=null)
            turnThisOffWhenPreviewing.SetActive(false); 
        // 5) tween to target position

        // scale
        previewGameObject.transform.localPosition = Vector3.zero;
        previewGameObject.transform.localScale = Vector3.one;
        previewGameObject.transform.DOScale(targetScale, animationTime).SetEase(Ease.OutQuint);

        // position
        if (IsChampionOnTheField)
        {
            Vector3 centeredPosition = new Vector3(0f, this.transform.position.y, 0f);
            previewGameObject.transform.DOMove(centeredPosition, animationTime).SetEase(Ease.OutQuint);
        }
        else
        {
            previewGameObject.transform.DOLocalMove(targetPosition, animationTime).SetEase(Ease.OutQuint);
        }
    }

    void StopThisPreview()
    {
        previewGameObject.SetActive(false);
        previewGameObject.transform.localScale = Vector3.one;
        previewGameObject.transform.localPosition = Vector3.zero;
        if (turnThisOffWhenPreviewing!=null)
            turnThisOffWhenPreviewing.SetActive(true); 
    }

    #endregion

    #region static methods
    private static void StopAllPreviews()
    {
        if (_currentPreview != null)
        {
            _currentPreview.StopThisPreview();
        }
    }

    private static bool IsPreviewingSomeCard()
    {
        if (!ArePreviewsAllowed)
            return false;

        HoverPreview[] allHoverBlowups = GameObject.FindObjectsOfType<HoverPreview>();

        foreach (HoverPreview hb in allHoverBlowups)
        {
            if (hb.IsOveringCollider && hb.IsThisPreviewEnabled)
                return true;
        }

        return false;
    }
    #endregion
   
}

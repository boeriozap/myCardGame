﻿using System;
using LitJson;
using System.Collections.Generic;


public static class Extensions
{
    public static List<T> GetListFromJsonData<T>(this JsonData jsonData, string key) where T:JsonDecodable, new()
    {
        var list = new List<T>();
        if (jsonData.Keys.Contains(key) && jsonData[key].IsArray)
        {
           for(int cardIndex=0; cardIndex<jsonData[key].Count; cardIndex++)
            {
                T newObj = new T();
                newObj.Init(jsonData[key][cardIndex]);
                list.Add(newObj);
            }
        }

        return list;
    }

    public static T GetValueFromJsonData<T>(this JsonData jsonData, string key) where T:JsonDecodable, new()
    {
        T newObj = new T();
        if (jsonData.Keys.Contains(key))
        {
            newObj.Init(jsonData[key]);
        }
        return newObj;
    }
}
﻿/*
 * MapIndex
 * Copyright (c) Alessio Zap Boerio
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapIndex
{
    public int col;
    public int row;

    public MapIndex(int col, int row)
    {
        this.col = col;
        this.row = row;
    }
}

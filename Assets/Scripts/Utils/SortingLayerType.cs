﻿/*
 * SortingLayerType
 * Copyright (c) Alessio Zap Boerio
 */

using UnityEngine;
using System.Collections;

public enum SortingLayerType
{
    Battleground,
    Cell
}


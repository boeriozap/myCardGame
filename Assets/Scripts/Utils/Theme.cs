﻿/*
 * Theme
 * Copyright (c) Alessio Zap Boerio
 */

using System;
using UnityEngine;

public static class Theme
{
    public static Color32 Green          { get { return new Color(r: 124f/255f, g: 152f/255f, b: 92f/255f); } }

    public static Color32 Red            { get { return new Color(r: 200f/255f, g: 100f/255f, b: 85f/255f); } }

    public static Color32 Purple         { get { return new Color(r: 133f/255f, g: 110f/255f, b: 165f/255f); } }

    public static Color32 White          { get { return new Color(r: 218f/255f, g: 205f/255f, b: 165f/255f); } }

    public static Color32 Black          { get { return new Color(r: 56f/255f,  g: 56f/255f,  b: 56f/255f); } }




    public static Color32 GetColorFor(ClanColor clan)
    {
        switch(clan)
        {
            case ClanColor.Green:   return Theme.Green;
            case ClanColor.Red:     return Theme.Red;
            case ClanColor.Purple:  return Theme.Purple;
            case ClanColor.White:   return Theme.White;
            default:                return Theme.Black;
        }
    }
}


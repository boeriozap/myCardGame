﻿/*
 * CardViewModel
 * Copyright (c) Alessio Zap Boerio
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// holds the refs to all the Text, Images on the card
public class CardViewModel : MonoBehaviour {

    public CardAsset cardAsset;
    public CardViewModel previewManager;
    [Header("Text Component References")]
    public Text nameText;
    public Text manaCostText;
    public Text powerText;
    public Text toughnessText;
    public Text levelText;
    public Text descriptionText;
    [Header("Image References")]
    public Image cardTopRibbonImage;
    public Image cardLowRibbonImage;
    public Image cardGraphicImage;
    public Image cardBodyImage;
    public Image cardGraphicFrameImage;
    public Image cardFaceFrameImage;
    public Image cardFaceGlowImage;
    public Image cardBackGlowImage;

    private bool _canBePlayed = false;
    public bool CanBePlayed
    {
        get
        {
            return _canBePlayed;
        }
        set
        {
            _canBePlayed = value;
            cardFaceGlowImage.enabled = value;
        }
    }

    #region life cycle

    void Awake()
    {
        if (cardAsset != null)
            ReadCardFromAsset();
    }

    #endregion

    #region public methods

    public void ReadCardFromAsset()
    {
        cardBodyImage.color         = Theme.GetColorFor(clan: cardAsset.clanColor);
        cardGraphicFrameImage.color = Theme.GetColorFor(clan: cardAsset.clanColor);
        cardFaceFrameImage.color    = Theme.GetColorFor(clan: cardAsset.clanColor);

        // 2) add card name
        nameText.text = cardAsset.name;
        // 3) add mana cost
        manaCostText.text = cardAsset.manaCost.ToString();
        // 4) add description
        descriptionText.text = cardAsset.description;
        // 5) Change the card graphic sprite
        cardGraphicImage.sprite = cardAsset.cardImage;

        if (cardAsset.maxToughness != 0)
        {
            // this is a creature
            powerText.text = cardAsset.power.ToString();
            toughnessText.text = cardAsset.maxToughness.ToString();
        }

        if (previewManager != null)
        {
            // this is a card and not a preview
            // Preview GameObject will have OneCardManager as well, but PreviewManager should be null there
            previewManager.cardAsset = cardAsset;
            previewManager.ReadCardFromAsset();
        }
    }

    #endregion

    #region private functions

    #endregion
}

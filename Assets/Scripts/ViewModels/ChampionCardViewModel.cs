﻿/*
 * ChampionCardViewModel
 * Copyright (c) Alessio Zap Boerio
 */

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChampionCardViewModel : MonoBehaviour 
{
    public int playerOwnerId;
    public MapCell currentCell;

    [Header("Card Infos")]
    public CardAsset cardAsset;
    public CardViewModel previewCard;
    [Header("Text Component References")]
    public Text levelText;
    public Text powerText;
    public Text toughnessText;
    [Header("Image References")]
    public Image championGraphicImage;
    public Image championGlowImage;

    private string _prefixLevel = "Lv.";

    void Awake()
    {
        if (cardAsset != null)
            ReadCreatureFromAsset();
    }

    private bool _canAttack = false;
    public bool CanAttack
    {
        get
        {
            return _canAttack;
        }

        set
        {
            _canAttack = value;

            championGlowImage.enabled = value;
        }
    }

    public void ReadCreatureFromAsset()
    {
        // Change the card graphic sprite
        championGraphicImage.sprite = cardAsset.cardImage;

        SetLevel(1);
        powerText.text = cardAsset.power.ToString();
        toughnessText.text = cardAsset.maxToughness.ToString();

        if (previewCard != null)
        {
            previewCard.cardAsset = cardAsset;
            previewCard.ReadCardFromAsset();
        }
    }	

    public void TakeDamage(int amount, int healthAfter)
    {
        if (amount > 0)
        {
//            DamageEffect.CreateDamageEffect(transform.position, amount);
            toughnessText.text = healthAfter.ToString();
        }
    }

    public void SetLevel(int level)
    {
        levelText.text = string.Format("{0}{1}", _prefixLevel, 1);
    }
}
